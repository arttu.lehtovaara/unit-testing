const expect = require('chai').expect
const assert = require('chai').assert
const should = require('chai').should()
const mylib = require('../src/mylib')

describe('unit testing mylib.js', () => {
  let text = undefined
  before(() => {
    text = 'Before() function started before test'
  })
  it('Should return 882 when using sum function with a=42, b=21', () => {
    const result = mylib.sum(42, 21)
    expect(result).to.equal(882)
  })
  it('Should return 31.5 when using average function with a=42, b=21', () => {
    const result = mylib.average(42, 21)
    expect(result).to.equal(31.5)
  })
  it('Assert Arttu is not Arjo', () => {
    const result = mylib.info('Arttu' !== 'Arjo')
    assert(result)
  })
  after(() => {
    console.log(text + ', this After() function show what is inside before() function')
  })
})