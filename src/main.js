const express = require('express')
const app = express()
const port = 3000
const mylib = require('./mylib')

// endpoint localhost:3000/
app.get('/', (req, res) => {
  res.send('Hello world!')
})

// endpoint localhost:3000/sum?a=42&b=21
app.get('/sum', (req, res) => {
  const a = parseInt(req.query.a)
  const b = parseInt(req.query.b)
  console.log({ a , b })
  const total = mylib.sum(a, b)
  res.send('sum works ' + total.toString())
})

// endpoint localhost:3000/average?a=42&b=21
app.get('/average', (req, res) => {
  const a = parseInt(req.query.a)
  const b = parseInt(req.query.b)
  console.log({ a, b })
  const total = mylib.average(a, b)
  res.send('average works ' + total.toString())
})

// endpoint localhost:3000/info?a=Arttu&b=Arjo
app.get('/info', (req, res) => {
  const a = req.query.a
  const b = req.query.b
  console.log({ a, b })
  const text = mylib.info(a, b)
  res.send(text)
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})