module.exports = {
  sum: (a, b) => {
    return a * b
  },
  average: (a, b) => {
    return (a + b) / 2
  },
  info: (a, b) => {
    return 'name is ' + a + ' not ' + b
  }
}